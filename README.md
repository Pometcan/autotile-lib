# AutoTile-Lib

---
> **Note:** This library is an older version and is still under development. Future updates will include additional features.

&nbsp;

## Usage

---

### `tilescale(<TileMap>, <Width>, <Height>, <Scale>, <Properties>)`

The `tilescale` function can be used in two different ways:

1. **Creating a Single TileMap:**
   - This method quickly creates a single tilemap. The size of the tilemap is calculated by the formula `(width * height) / scale`.

   ```lua
   local tilemap = {}
   tilemap = tilescale(tilemap, 100, 100, 32, {sprite})
   ```

   **Output:**

   ```lua
   {
       {0, 0, 0},
       {0, 0, 0},
       {0, 0, 0}
   }
   ```

2. **Creating Multiple TileMaps:**
   - This method allows you to create multiple tilemaps and retrieve the created tilemap if needed.

   ```lua
   tilescale(false, 100, 100, 32, {sprite})
   -- or
   local tilemap = {}
   tilemap = tilescale(false, 100, 100, 32, {sprite})
   ```

**Properties Table:**

```lua
{
    Location_X = <integer>,  -- Sets the starting X position of the tile.
    Location_Y = <integer>,  -- Sets the starting Y position of the tile.
    Sprite = <img>,  -- Specifies the image to use for filling the tile. (Currently supports only a single image.)
    Spritechange = <bool>  -- Enables sprite change (MUST be a table of sprites, e.g., {sprite1, sprite2, sprite3, ...})
}
```

**Sprites Should Be Referenced As Shown Below:**
![ref1](demo/imgs/tile1.png) ![ref2](demo/imgs/tile2.png) ![ref3](demo/imgs/tile3.png)

### Remaining Functions

- `tiledraw()`, `tilemouse()`, `tilekeyboard()`

These functions should be placed in the appropriate callbacks:

```lua
love.draw = function()
    tiledraw()
end

love.mousepressed = function(x, y, btn)
    tilemouse(x, y, btn)
end

love.keyboardpressed = function(key)
    tilekeyboard(key)
end
```

## To-Do List

### Sprite

- [ ] **Different Tiling Modes:**
  - Add various tilemap editing options and styles.

- [ ] **User Interface (UI):**
  - Create a user-friendly interface for tilemap editing and management.

- [ ] **Advanced Sprite Features:**
  - Grouping and naming sprites.
  - Effects: Background removal, brightness and contrast adjustments, etc.

### Animation

- [ ] **State Machine:**
  - Implement a system for transitioning between animation states.

- [ ] **Detailed Frame Settings:**
  - Frame rates, transition times, and loop settings.

- [ ] **User Interface (UI):**
  - Tools for animation editing and previewing.

### Additional Features

- [ ] **Editor Support:**
  - Visual editor and sprite creator.

- [ ] **Performance Enhancements:**
  - Memory management and CPU optimizations.

- [ ] **Documentation:**
  - Comprehensive documentation with usage guides and examples.
